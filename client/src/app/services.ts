import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class titleService {

cursor = 0;

constructor (private HttpClient: HttpClient) {};

getTitles (config ={}) : Promise <any> {
    let qs = new HttpParams ()
    .set ('limit', config ['limit'] || 10)

    return (
        this.HttpClient.get (
            'http://localhost:3000/books', { params: qs })
            .take(1).toPromise()
        )
}
getDetails(Id: number): Promise<any> {
    return (
      this.HttpClient.get(`http://localhost:3000/books'/books/${Id}`)
        .take(1).toPromise()
    );
  }


}