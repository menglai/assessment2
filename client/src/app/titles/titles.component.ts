import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { titleService } from '../services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './titles.component.html',
  styleUrls: ['./titles.component.css']
})
export class TitlesComponent implements OnInit {
  title = 'app';

  @ViewChild(NgForm) bookForm: NgForm;

  titles: string;
  author: string;
  listTitle = [];
  private limit = 10;

  constructor (private titleSvc: titleService, private router: Router,
    private activatedRoute: ActivatedRoute){};

  ngOnInit(){
    /*this.titleSvc.getTitles ({limit: this.limit})
    .then ((result) => {this.titles = result})
    .catch ((error) => { console.log (error); });*/

  }

  search(bookForm: NgForm){

    this.titleSvc.getTitles ({limit:this.limit})
    .then ((result) => {this.titles = result})
    .catch ((error) => { console.log (error); });

  }

  showDetails(Id: number) {
    console.log('> Id: %d', Id);
    this.router.navigate([ '/books', Id ]);
  }

  
}