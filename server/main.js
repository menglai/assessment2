const express = require ('express');
const mysql = require ('mysql');
const dbConfig = require ('./dbConfig')
const cors = require ('cors')
const bodyParser = require('body-parser');

const pool = mysql.createPool (dbConfig);

const app = express ();

app.use(cors())

const author_first_last_title = `select title, author_firstname, author_lastname from books order by title limit ? offset ?`;

app.get (`/books`, (req, resp)=> {
    const limit = parseInt(req.query.limit) || 10;
    const offset = parseInt(req.query.offset) || 0;

    pool.getConnection ((err, conn)=>{
        if (err) {
            resp.status(500).json ({error: err});
            return;
        }
        conn.query (author_first_last_title, [limit, offset], (err, results)=>{
            try {
                if (err) {
                    resp.status (400).json ({error: err});
                    console.log (`> query error:`, err)
                    return;
                    } else {
                    resp.status (200)
                    resp.send(results);
                    }
            }
                finally {
                    conn.release()
                };
        })

    })

})

const Select_Title = 'select * from books where id = ?';
app.get ('/books/:id', (req, resp) => {
    pool.getConnection ((error, conn) =>{
        if (error) {
            resp.status (500).json ({error: error});
            return
        }
        conn.query(Select_Title, [req.params.id],
            (error, result) =>{
                try {
                    if (error) {
                        resp.status(400).json({error: error}); 
                        return;
                    }
                    if (result.length)
                        resp.status(200).json(result[0]);
                    else
                        resp.status(404).json({error: 'Not Found'});
                } finally { conn.release() ;}
            }
        )

    })
})


const PORT = parseInt(process.argv[2]) || process.env.APP_PORT || 3000;

console.log('Pinging database...');

pool.getConnection((err, conn) => {
    if (err) {
        console.error('>Error: ', err);
        process.exit(-1);
    }

    conn.ping((err) => {
        if (err) {
            console.error('>Cannot ping: ', err);
            process.exit(-1);
        }
        conn.release();

        //Start application only if we can ping database
        app.listen(PORT, () => {
            console.log('Starting application on port %d', PORT);
        });
    });
});