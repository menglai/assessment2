import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { titleService } from './services';
import { ComponentComponent } from './component.component';
import { TitlesComponent } from './titles/titles.component';

const routes: Routes = [
  { path: '', component: TitlesComponent },
  { path: 'books', component: TitlesComponent },
  { path: 'books/:Id', component: ComponentComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    ComponentComponent,
    TitlesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ titleService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
