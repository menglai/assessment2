import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { titleService } from './services';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styleUrls: ['./component.component.css']
})
export class ComponentComponent implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private titleSvc: titleService) { }

  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['/']);
  }

}
